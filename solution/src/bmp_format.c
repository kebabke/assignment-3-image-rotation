#include "../include/bmp_format.h"

size_t compute_padding(size_t w) {
    size_t res = w * sizeof(struct pixel) % 4;
    return res ? 4 - res : 0;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    size_t h = img->height;
    size_t w = img->width;
    struct pixel* data = (struct pixel*) malloc(sizeof(struct pixel) * h * w);
    if (data == NULL) {
        fprintf(stderr, "Allocation error\n");
        return READ_OUT_OF_MEMORY;
    }
    size_t padding = compute_padding(w);
    size_t chars;
    for (size_t i = 0; i < h; i++) {
        chars = fread(data + (i * w), sizeof(struct pixel), w, in);
        if (chars < w) {
            free(data);
            fprintf(stderr, "The given input BMP file is corrupted\n");
            return READ_INVALID_BITS;
        }
        chars = (size_t)fseek(in, (long)padding, SEEK_CUR);
        if (chars) {
            free(data);
            fprintf(stderr, "The given input BMP file is corrupted");
            return READ_INVALID_BITS;
        }
    }
    img->data = data;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    size_t h, w, padding;
    h = img->height;
    w = img->width;
    padding = compute_padding(w);
    size_t chars;
    for (size_t i = 0; i < h; i++) {
        chars = fwrite(img->data + (i * w), sizeof(struct pixel), w, out);
        if (chars < sizeof(struct pixel)) {
            fprintf(stderr, "Image could not be written to the output file\n");
            return WRITE_ERROR;
        }
        chars = (size_t)fseek(out, (long)padding, SEEK_CUR);
        if (chars) {
            fprintf(stderr, "Image could not be written to the output file\n");
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
