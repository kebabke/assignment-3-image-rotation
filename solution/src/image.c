#include "../include/image.h"

enum rotate_status rotate(struct image* image) {
    size_t h = image->height;
    size_t w = image->width;
    struct pixel* c = (struct pixel*) malloc(sizeof(struct pixel) * h * w);
    if (c == NULL) {
        fprintf(stderr, "There is not enough memory to complete the operation\n");
        return RS_ERROR;
    }
    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            c[j * h + (h - 1 - i)] = image->data[i * w + j];
        }
    }
    struct pixel* temp = image->data;
    image->data = c;
    free(temp);
    size_t nh = image->width, nw = image->height;
    image->height = nh;
    image->width = nw;
    return RS_OK;
}
