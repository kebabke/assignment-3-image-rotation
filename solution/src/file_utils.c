#include "../include/file_utils.h"

struct image read_image(FILE *in, enum type imageType) {
  struct image image;
  image.data = NULL;
  if (imageType == BMP) {
    struct read_return header = read_BMP_Header(in);
    if (header.ans == !READ_OK) {
      goto function_end;
    }
    image.height = header.header.biHeight;
    image.width = header.header.biWidth;
    from_bmp(in, &image);
  } // else more types
function_end:
  fclose(in);
  return image;
}

enum write_status write_image(FILE *out, enum type imageType,
                                     struct image *image) {
  enum write_status ans = WRITE_OK;
  if (imageType == BMP) {
    enum write_status temp = write_BMP_Header(out, image);
    if (temp != WRITE_OK) {
      goto function_end;
    }
    temp = to_bmp(out, image);
    if (temp != WRITE_OK) {
      goto function_end;
    }
  }
function_end:
  fclose(out);
  return ans;
}
