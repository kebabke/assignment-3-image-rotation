#include "../include/utils.h"

struct read_return read_BMP_Header(FILE *in) {
  struct bmp_header return_header;
  size_t bytes = fread(&return_header, 1, sizeof(struct bmp_header), in);
  struct read_return return_value = {.header = return_header, .ans = READ_OK};
  if (bytes < sizeof(struct bmp_header)) {
    fprintf(stderr, "The given file is not a BMP file\n");
    return_value.ans = READ_INVALID_HEADER;
    return return_value;
  }
  // error validations
  fseek(in, return_header.bOffBits, SEEK_SET);
  return return_value;
}

enum write_status write_BMP_Header(FILE *out, struct image* image) {
    struct bmp_header output_bmp;
    output_bmp.bfType = 0x4d42;
    output_bmp.bfReserved = 0;
    output_bmp.bOffBits = 54;
    output_bmp.biSize = 40;
    output_bmp.biWidth = image->width;
    output_bmp.biHeight = image->height;
    output_bmp.biPlanes = 1;
    output_bmp.biBitCount = 24;
    output_bmp.biCompression = 0;
    output_bmp.biSizeImage = image->height * (image->width * sizeof(struct pixel) + compute_padding(image->width));
    output_bmp.biXPelsPerMeter = 0;
    output_bmp.biYPelsPerMeter = 0;
    output_bmp.biClrUsed = 0;
    output_bmp.biClrImportant = 0;
    output_bmp.bfileSize = output_bmp.biSize + output_bmp.biSizeImage;
    size_t chars = fwrite(&output_bmp, 1, sizeof(struct bmp_header), out);
    if (chars < sizeof(struct bmp_header)) {
        fprintf(stderr, "Image could not be written to the output file\n");
        return WRITE_ERROR;
    }
    chars = fseek(out, output_bmp.bOffBits, SEEK_SET);
    if (chars) {
        fprintf(stderr, "Image could not be written to the output file\n");
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

