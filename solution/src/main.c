#include "../include/file_utils.h"

int main(int argc, char **argv) {
  (void)argc;
  (void)argv; // supress 'unused parameters' warning

  if (argc < 3) {
    fprintf(stderr, "Incorrect number of arguments\n");
    goto exit_bad;
  }
  FILE *in = NULL;
  in = fopen(argv[1], "rb");
  if (in == NULL) {
    fprintf(stderr, "Input file not found\n");
    goto exit_bad;
  }
  struct image image = read_image(in, BMP);
  if (image.data == NULL) {
    goto exit_bad;
  }
  enum rotate_status rotateAns = rotate(&image);
  if (rotateAns != RS_OK) {
    free(image.data);
    goto exit_bad;
  }
  FILE *out = NULL;
  out = fopen(argv[2], "wb");
  enum write_status writeAns = write_image(out, BMP, &image);
  free(image.data);
  if (writeAns != WRITE_OK) {
    goto exit_bad;
  }
  goto exit_good;
exit_bad:
    printf("все рухнуло\n");
    return 1;
exit_good:
    printf("Всё хорошо\n");
    return 0;
}
