#ifndef IMAGE
#define IMAGE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum type {
    BMP
    // more types
};

enum rotate_status {
    RS_OK,
    RS_ERROR
};

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

enum rotate_status rotate(struct image* image);

#endif
