#ifndef FILE_UTILS
#define FILE_UTILS
#include "utils.h"

struct image read_image(FILE *in, enum type imageType);

enum write_status write_image(FILE *out, enum type imageType,
                                     struct image *image);
#endif
