#ifndef UTILS
#define UTILS
#include "bmp_format.h"

struct read_return {
  struct bmp_header header;
  enum read_status ans;
};

struct read_return read_BMP_Header(FILE *in);

enum write_status write_BMP_Header(FILE *out, struct image* image);
#endif
